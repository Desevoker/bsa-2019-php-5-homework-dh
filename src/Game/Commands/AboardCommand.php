<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Commands\Command;
use BinaryStudioAcademy\Game\Contracts\Helpers\State;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Helpers\World;

class AboardCommand implements Command
{
    private $enemy;
    private $gameState;
    private $message;
    private $player;

    public function __construct(State $gameState)
    {
        $this->gameState = $gameState;
    }

    public function execute(Reader $reader, Writer $writer)
    {
        $this->prepare();

        $this->gameState->setPlayer($this->player);
        $this->gameState->setEnemy($this->enemy);

        $writer->write($this->message);
    }

    private function prepare()
    {
        if ($this->gameState->getLocation()['harbor'] === World::HOME) {
            throw new \LogicException('There is no ship to aboard');
        }

        $this->player = $this->gameState->getPlayer();
        $this->enemy = $this->gameState->getEnemy();

        if ($this->enemy->isAlive()) {
            throw new \LogicException('You cannot board this ship, since it has not yet sunk');
        }

        $playerHold = $this->player->getHold();
        $enemyHold = $this->enemy->getHold();

        if (in_array('💰', $enemyHold) || in_array('🍾', $enemyHold)) {
            $freeSpace = array_search('_', $playerHold);

            if ($freeSpace !== false) {
                $loot = $enemyHold[0];
                $playerHold[$freeSpace] = $loot;
                rsort($playerHold, SORT_STRING);
                $enemyHold[0] = '_';
                rsort($enemyHold, SORT_STRING);

                $this->player->setHold($playerHold);
                $this->enemy->setHold($enemyHold);

                $this->message = "You got {$loot}." . PHP_EOL;
            } else {
                $this->message = "Can't take loot, your ship hold is full." . PHP_EOL;
            }
        } else {
            $this->message = "{$this->enemy->getName()}'s hold is empty." . PHP_EOL;
        }
    }
}

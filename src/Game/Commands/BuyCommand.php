<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Commands\Command;
use BinaryStudioAcademy\Game\Contracts\Helpers\State;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Helpers\World;

class BuyCommand implements Command
{
    private $gameState;
    private $message;
    private $option;
    private $player;
    private $maxStrength = World::MAX_STRENGTH;
    private $maxArmour = World::MAX_ARMOUR;
    private $maxLuck = World::MAX_LUCK;

    public function __construct(State $gameState, string $option)
    {
        $this->gameState = $gameState;
        $this->option = $option;
    }

    public function execute(Reader $reader, Writer $writer)
    {
        $this->prepare();

        $this->gameState->setPlayer($this->player);

        $writer->write($this->message);
    }

    private function prepare()
    {
        if ($this->gameState->getLocation()['harbor'] !== World::HOME) {
            throw new \LogicException('You can trade only in Pirates Harbor.');
        }

        $this->player = $this->gameState->getPlayer();
        $playerHold = $this->player->getHold();
        $gold = array_search('💰', $playerHold);

        if ($gold !== false) {
            switch ($this->option) {
                case 'rum':
                    $playerHold[$gold] = '🍾';
                    $rumBottles = array_count_values($playerHold)['🍾'];
                    rsort($playerHold, SORT_STRING);

                    $this->player->setHold($playerHold);
                    $this->message = "You've bought a rum. Your hold contains {$rumBottles} bottle(s) of rum." . PHP_EOL;
                    break;

                case 'strength':
                    $nextValue = $this->player->getStrength() + 1;

                    if ($nextValue > $this->maxStrength) {
                        $this->message = "Can't buy {$this->option}, it is at max value." . PHP_EOL;
                    } else {
                        $this->player->setStrength($nextValue);
                        $playerHold[$gold] = '_';
                        $this->player->setHold($playerHold);
                        $this->message = "You've bought a {$this->option}. Your {$this->option} is {$nextValue}." . PHP_EOL;
                    }

                    break;

                case 'armour':
                    $nextValue = $this->player->getArmour() + 1;

                    if ($nextValue > $this->maxArmour) {
                        $this->message = "Can't buy {$this->option}, it is at max value already." . PHP_EOL;
                    } else {
                        $this->player->setArmour($nextValue);
                        $playerHold[$gold] = '_';
                        $this->player->setHold($playerHold);
                        $this->message = "You've bought a {$this->option}. Your {$this->option} is {$nextValue}." . PHP_EOL;
                    }

                    break;

                case 'luck':
                    $nextValue = $this->player->getLuck() + 1;

                    if ($nextValue > $this->maxLuck) {
                        $this->message = "Can't buy {$this->option}, it is at max value already." . PHP_EOL;
                    } else {
                        $this->player->setLuck($nextValue);
                        $playerHold[$gold] = '_';
                        $this->player->setHold($playerHold);
                        $this->message = "You've bought a {$this->option}. Your {$this->option} is {$nextValue}." . PHP_EOL;
                    }

                    break;
            }
        } else {
            $this->message = "Can't buy {$this->option}, not enough gold." . PHP_EOL;
        }
    }
}

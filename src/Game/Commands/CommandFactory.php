<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Commands\Command;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Contracts\Helpers\State;
use BinaryStudioAcademy\Game\Helpers\World;

class CommandFactory
{
    private $commands;
    private $gameState;
    private $random;

    public function __construct(State $gameState, Random $random)
    {
        $this->commands = World::COMMANDS;
        $this->gameState = $gameState;
        $this->random = $random;
    }

    public function buildCommand(string $command): Command
    {
        if (preg_match('/(set-sail|buy)\s*\w*/', $command)) {
            $pieces = explode(' ', $command);
            $command = $pieces[0];
            $option = $pieces[1] ?? null;

            if ($option === null) {
                throw new \InvalidArgumentException(
                    "Usage: {$command} <" . join('|', $this->commands[$command]['options']) . '>'
                );
            }

            switch ($command) {
                case 'set-sail':
                    if (in_array($option, $this->commands['set-sail']['options'])) {
                        return new SetSailCommand($this->gameState, $option, $this->random);
                    }

                    throw new \InvalidArgumentException(
                        "Direction '{$option}' incorrect, choose from: " . join(', ', $this->commands['set-sail']['options'])
                    );

                case 'buy':
                    if (in_array($option, $this->commands['buy']['options'])) {
                        return new BuyCommand($this->gameState, $option);
                    }

                    throw new \InvalidArgumentException(
                        "Option '{$option}' incorrect, choose from: " . join(', ', $this->commands['buy']['options'])
                    );
            }
        }

        switch ($command) {
            case 'fire':
                return new FireCommand($this->gameState, $this->random);
            case 'aboard':
                return new AboardCommand($this->gameState);
            case 'drink':
                return new DrinkCommand($this->gameState);
            case 'help':
                return new HelpCommand($this->commands);
            case 'stats':
                return new StatsCommand($this->gameState->getPlayer());
            case 'whereami':
                return new WhereamiCommand($this->gameState->getLocation());
            case 'exit':
                return new ExitCommand();
        }

        throw new \InvalidArgumentException("Command '{$command}' not found");
    }
}

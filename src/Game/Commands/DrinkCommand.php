<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Commands\Command;
use BinaryStudioAcademy\Game\Contracts\Helpers\State;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Helpers\World;

class DrinkCommand implements Command
{
    private $gameState;
    private $message;
    private $player;

    public function __construct(State $gameState)
    {
        $this->gameState = $gameState;
    }

    public function execute(Reader $reader, Writer $writer)
    {
        $this->prepare();

        $this->gameState->setPlayer($this->player);

        $writer->write($this->message);
    }

    private function prepare()
    {
        $this->player = $this->gameState->getPlayer();
        $playerHold = $this->player->getHold();
        $rum = array_search('🍾', $playerHold);

        if ($rum !== false) {
            if ($this->player->getHealth() === World::MAX_HEALTH) {
                $this->message = "You're at full health" . PHP_EOL;
            } else {
                $playerHold[$rum] = '_';
                rsort($playerHold, SORT_STRING);
                $this->player->setHold($playerHold);
                $this->player->heal(World::RUM_HEALING);

                $this->message = "You've drunk a rum and your health filled to {$this->player->getHealth()}" . PHP_EOL;
            }
        } else {
            $this->message = "You don't have a rum to drink :(" . PHP_EOL;
        }
    }
}

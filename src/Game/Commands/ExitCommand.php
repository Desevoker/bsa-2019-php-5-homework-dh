<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Commands\Command;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class ExitCommand implements Command
{
    public function execute(Reader $reader, Writer $writer)
    {
        $writer->writeln('Do you really want to exit? <No|Yes>');
        $input = $reader->read();

        if ($input === 'yes' || $input === 'y') {
            $writer->writeln('See you later!');
            exit;
        }

        $writer->writeln('Ok :)');
    }
}

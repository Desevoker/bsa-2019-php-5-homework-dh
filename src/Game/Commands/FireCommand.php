<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Commands\Command;
use BinaryStudioAcademy\Game\Contracts\Helpers\State;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Helpers\Map;
use BinaryStudioAcademy\Game\Helpers\Math;
use BinaryStudioAcademy\Game\Helpers\World;

class FireCommand implements Command
{
    private $enemy;
    private $gameState;
    private $location;
    private $message;
    private $player;
    private $random;

    public function __construct(State $gameState, Random $random)
    {
        $this->gameState = $gameState;
        $this->random = $random;
    }

    public function execute(Reader $reader, Writer $writer)
    {
        $this->prepare();

        $this->gameState->setPlayer($this->player);
        $this->gameState->setLocation($this->location);
        $this->gameState->setEnemy($this->enemy);

        $writer->write($this->message);
    }

    private function prepare()
    {
        if ($this->gameState->getLocation()['harbor'] === World::HOME) {
            throw new \LogicException('There is no ship to fight');
        }

        $this->player = $this->gameState->getPlayer();
        $this->location = $this->gameState->getLocation();
        $this->enemy = $this->gameState->getEnemy();

        $playerLucky = Math::luck($this->random, $this->player->getLuck());
        $enemyLucky = Math::luck($this->random, $this->enemy->getLuck());
        $playerDamage = Math::damage($this->player->getStrength(), $this->enemy->getArmour());
        $enemyDamage = Math::damage($this->enemy->getStrength(), $this->player->getArmour());

        $this->message = '';
        if ($playerLucky) {
            $this->enemy->receiveDamage($playerDamage);

            $this->message .= "{$this->enemy->getName()} has damaged on: {$playerDamage} points." . PHP_EOL;
            $this->message .= "health: {$this->enemy->getHealth()}" . PHP_EOL;
        } else {
            $this->message .= "You missed on {$this->enemy->getName()}." . PHP_EOL;
        }

        if ($this->enemy->isDead()) {
            if ($this->enemy->getName() === 'HMS Royal Sovereign') {
                $winMessage = '🎉🎉🎉Congratulations🎉🎉🎉' . PHP_EOL;
                $winMessage .= '💰💰💰 All gold and rum of Great Britain belong to you! 🍾🍾🍾' . PHP_EOL;

                throw new \RuntimeException($winMessage);
            }

            $this->win();
        } else {
            if ($enemyLucky) {
                $this->player->receiveDamage($enemyDamage);

                $this->message .= "{$this->enemy->getName()} damaged your ship on: {$enemyDamage} points." . PHP_EOL;
                $this->message .= "health: {$this->player->getHealth()}" . PHP_EOL;
            } else {
                $this->message .= "{$this->enemy->getName()} missed." . PHP_EOL;
            }

            if ($this->player->isDead()) {
                $this->die();
            }
        }
    }

    private function win()
    {
        $this->message = "{$this->enemy->getName()} on fire. Take it to the boarding." . PHP_EOL;
    }

    private function die()
    {
        $this->location = Map::getLocationById(World::HOME);

        $strength = $this->player->getStrength() - 1;
        $armour = $this->player->getArmour() - 1;
        $luck = $this->player->getLuck() - 1;

        if ($strength === 0 || $armour === 0 || $luck === 0) {
            exit('Your ship has been sunk. Game over.');
        }

        $this->player->setStrength($strength);
        $this->player->setArmour($armour);
        $this->player->setLuck($luck);
        $this->player->heal();
        $this->player->setHold(['_', '_', '_']);

        $this->message = 'Your ship has been sunk.' . PHP_EOL;
        $this->message .= "You restored in the {$this->location['name']}." . PHP_EOL;
        $this->message .= 'You lost all your possessions and 1 of each stats.' . PHP_EOL;
    }
}

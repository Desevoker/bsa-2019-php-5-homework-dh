<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Commands\Command;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class HelpCommand implements Command
{
    private $commands;
    private $message;

    public function __construct(array $commands)
    {
        $this->commands = $commands;
    }

    public function execute(Reader $reader, Writer $writer)
    {
        $this->prepare();
        $writer->write($this->message);
    }

    private function prepare()
    {
        $this->message = 'List of commands:' . PHP_EOL;

        foreach ($this->commands as $command => $description) {
            if (is_array($description)) {
                $this->message .= "{$command} <" . join('|', $description['options']) . "> - {$description['description']}" . PHP_EOL;
            } else {
                $this->message .= "{$command} - {$description}" . PHP_EOL;
            }
        }
    }
}

<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Commands\Command;
use BinaryStudioAcademy\Game\Contracts\Helpers\State;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Helpers\Map;
use BinaryStudioAcademy\Game\Helpers\World;
use BinaryStudioAcademy\Game\Ships\ShipBuilder;

class SetSailCommand implements Command
{
    private $direction;
    private $gameState;
    private $message;
    private $newLocation;
    private $newEnemy;
    private $player;
    private $random;

    public function __construct(State $gameState, string $direction, Random $random)
    {
        $this->gameState = $gameState;
        $this->direction = $direction;
        $this->random = $random;
    }

    public function execute(Reader $reader, Writer $writer)
    {
        $this->prepare();

        $this->gameState->setPlayer($this->player);
        $this->gameState->setLocation($this->newLocation);
        $this->gameState->setEnemy($this->newEnemy);

        $writer->write($this->message);
    }

    private function prepare()
    {
        $this->player = $this->gameState->getPlayer();
        $this->newLocation = Map::getLocationByDirection($this->gameState->getLocation(), $this->direction);

        $this->message = "Harbor {$this->newLocation['harbor']}: {$this->newLocation['name']}." . PHP_EOL;

        if ($this->newLocation['harbor'] === World::HOME) {
            $this->newEnemy = null;

            if ($this->player->getHealth() < World::PLAYER_BASE_HEALTH) {
                $this->player->heal();

                $this->message .= "Your health is repared to {$this->player->getHealth()}." . PHP_EOL;
            }
        } else {
            $shipBuilder = new ShipBuilder($this->random);
            $this->newEnemy = $shipBuilder->buildShip($this->newLocation['enemy']);

            $this->message .= "You see {$this->newEnemy->getName()}:" . PHP_EOL;
            $this->message .= "strength: {$this->newEnemy->getStrength()}" . PHP_EOL;
            $this->message .= "armour: {$this->newEnemy->getArmour()}" . PHP_EOL;
            $this->message .= "luck: {$this->newEnemy->getLuck()}" . PHP_EOL;
            $this->message .= "health: {$this->newEnemy->getHealth()}" . PHP_EOL;
        }
    }
}

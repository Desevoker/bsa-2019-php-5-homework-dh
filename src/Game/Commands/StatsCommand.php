<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Commands\Command;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Ships\Ship;

class StatsCommand implements Command
{
    private $message;
    private $player;

    public function __construct(Ship $player)
    {
        $this->player = $player;
    }

    public function execute(Reader $reader, Writer $writer)
    {
        $this->prepare();
        $writer->write($this->message);
    }

    private function prepare()
    {
        $this->message = 'Ship stats:' . PHP_EOL;
        $this->message .= "strength: {$this->player->getStrength()}" . PHP_EOL;
        $this->message .= "armour: {$this->player->getArmour()}" . PHP_EOL;
        $this->message .= "luck: {$this->player->getLuck()}" . PHP_EOL;
        $this->message .= "health: {$this->player->getHealth()}" . PHP_EOL;
        $this->message .= 'hold: [ ' . join(' ', $this->player->getHold()) . ' ]' . PHP_EOL;
    }
}

<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Commands\Command;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class WhereamiCommand implements Command
{
    private $location;
    private $message;

    public function __construct(array $location)
    {
        $this->location = $location;
    }

    public function execute(Reader $reader, Writer $writer)
    {
        $this->prepare();
        $writer->write($this->message);
    }

    private function prepare()
    {
        $this->message = "Harbor {$this->location['harbor']}: {$this->location['name']}." . PHP_EOL;
    }
}

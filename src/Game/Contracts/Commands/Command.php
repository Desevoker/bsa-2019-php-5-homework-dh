<?php

namespace BinaryStudioAcademy\Game\Contracts\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

interface Command
{
    public function execute(Reader $reader, Writer $writer);
}

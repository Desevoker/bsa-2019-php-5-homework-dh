<?php

namespace BinaryStudioAcademy\Game\Contracts\Helpers;

interface Math
{
    public static function luck(Random $random, int $luck): bool;

    public static function damage(int $strength, int $armour): int;
}

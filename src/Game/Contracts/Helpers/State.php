<?php

namespace BinaryStudioAcademy\Game\Contracts\Helpers;

use BinaryStudioAcademy\Game\Contracts\Ships\Ship;

interface State
{
    public function getPlayer(): Ship;

    public function getLocation(): array;

    public function getEnemy(): ?Ship;

    public function setPlayer(Ship $player): void;

    public function setLocation(array $location): void;

    public function setEnemy(?Ship $enemy): void;
}

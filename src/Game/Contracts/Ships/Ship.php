<?php

namespace BinaryStudioAcademy\Game\Contracts\Ships;

interface Ship
{
    public function getName(): string;

    public function getStrength(): int;

    public function getArmour(): int;

    public function getLuck(): int;

    public function getHealth(): int;

    public function getHold(): array;

    public function setName(string $name): void;

    public function setStrength(int $strength): void;

    public function setArmour(int $armour): void;

    public function setLuck(int $luck): void;

    public function setHealth(int $health): void;

    public function setHold(array $hold): void;

    public function heal(?int $health): void;

    public function receiveDamage(int $damage): void;

    public function isAlive(): bool;

    public function isDead(): bool;
}

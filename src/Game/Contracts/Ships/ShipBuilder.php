<?php

namespace BinaryStudioAcademy\Game\Contracts\Ships;

interface ShipBuilder
{
    public function buildShip(string $type): Ship;
}

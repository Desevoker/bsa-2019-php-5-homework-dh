<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Contracts\Helpers\State;
use BinaryStudioAcademy\Game\Commands\CommandFactory;
use BinaryStudioAcademy\Game\Helpers\GameState;
use BinaryStudioAcademy\Game\Helpers\World;
use BinaryStudioAcademy\Game\Helpers\Map;
use BinaryStudioAcademy\Game\Ships\ShipBuilder;

class Game
{
    private $commandFactory;
    private $gameState;
    private $random;

    public function __construct(Random $random)
    {
        $this->random = $random;
    }

    public function start(Reader $reader, Writer $writer)
    {
        $writer->writeln('Welcome to the "Battle Ship" game!');
        $writer->writeln('Type <help> for a list of commands.');
        $writer->writeln('Adventure has begun. Wish you good luck!');

        try {
            $this->deleteState();
            $this->bootstrap();
            while (true) {
                $this->step($reader, $writer);
            }
        } catch (\RuntimeException $e) {
            $writer->writeln($e->getMessage());
        }
    }

    public function run(Reader $reader, Writer $writer)
    {
        try {
            $this->bootstrap($this->restoreState());
            $this->step($reader, $writer);
            $this->saveState();
        } catch (\RuntimeException $e) {
            $writer->writeln($e->getMessage());
            $this->deleteState();
        }
    }

    private function bootstrap(?State $savedState = null)
    {
        $shipBuilder = new ShipBuilder($this->random);

        if ($savedState === null) {
            $player = $shipBuilder->buildShip('player');
            $location = Map::getLocationById(World::HOME);
            $this->gameState = new GameState($player, $location);
        } else {
            $this->gameState = $savedState;
        }

        $this->commandFactory = new CommandFactory($this->gameState, $this->random);
    }

    private function step(Reader $reader, Writer $writer)
    {
        $input = $reader->read();

        try {
            $command = $this->commandFactory->buildCommand($input);
            $command->execute($reader, $writer);
        } catch (\InvalidArgumentException | \LogicException $e) {
            $writer->writeln($e->getMessage());
        }
    }

    private function restoreState()
    {
        if (file_exists('gamesave')) {
            return unserialize(file_get_contents('gamesave'));
        }

        return null;
    }

    private function saveState()
    {
        file_put_contents('gamesave', serialize($this->gameState));
    }

    private function deleteState()
    {
        if (file_exists('gamesave')) {
            unlink('gamesave');
        }
    }
}

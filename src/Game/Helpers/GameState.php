<?php

namespace BinaryStudioAcademy\Game\Helpers;

use BinaryStudioAcademy\Game\Contracts\Helpers\State;
use BinaryStudioAcademy\Game\Contracts\Ships\Ship;

class GameState implements State
{
    private $player;
    private $location;
    private $enemy;

    public function __construct(Ship $player, array $location, ?Ship $enemy = null)
    {
        $this->player = $player;
        $this->location = $location;
        $this->enemy = $enemy;
    }

    public function getPlayer(): Ship
    {
        return $this->player;
    }

    public function getLocation(): array
    {
        return $this->location;
    }

    public function getEnemy(): ?Ship
    {
        return $this->enemy;
    }

    public function setPlayer(Ship $player): void
    {
        $this->player = $player;
    }

    public function setLocation(array $location): void
    {
        $this->location = $location;
    }

    public function setEnemy(?Ship $enemy): void
    {
        $this->enemy = $enemy;
    }
}

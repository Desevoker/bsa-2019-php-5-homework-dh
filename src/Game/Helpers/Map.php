<?php

namespace BinaryStudioAcademy\Game\Helpers;

final class Map
{
    public static function getLocationById(int $id): array
    {
        return World::MAP[$id];
    }

    public static function getLocationByDirection(array $currentLocation, string $direction): array
    {
        $id = $currentLocation['sail'][$direction];

        if ($id === 0) {
            throw new \InvalidArgumentException('Harbor not found in this direction');
        }

        return self::getLocationById($id);
    }
}

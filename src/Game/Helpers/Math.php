<?php

namespace BinaryStudioAcademy\Game\Helpers;

use BinaryStudioAcademy\Game\Contracts\Helpers\Math as IMath;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;

final class Math implements IMath
{
    private const DAMAGE_COEFFICIENT = 10;

    public static function luck(Random $random, int $luck): bool
    {
        return ceil($random->get() * World::MAX_LUCK) > (World::MAX_LUCK - $luck);
    }

    public static function damage(int $strength, int $armour): int
    {
        return ceil(self::DAMAGE_COEFFICIENT * $strength / $armour);
    }
}

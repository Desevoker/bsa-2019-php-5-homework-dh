<?php

namespace BinaryStudioAcademy\Game\Helpers;

final class World
{
    public const COMMANDS = [
        'help' => 'shows this list of commands',
        'stats' => 'shows stats of ship',
        'set-sail' => [
            'options' => [
                'east',
                'west',
                'north',
                'south'
            ],
            'description' => 'moves in given direction'
        ],
        'fire' => 'attacks enemy\'s ship',
        'aboard' => 'collect loot from the ship',
        'buy' => [
            'options' => [
                'strength',
                'armour',
                'luck',
                'rum'
            ],
            'description' => 'buys skill or rum: 1 chest of gold - 1 item'
        ],
        'drink' => 'your captain drinks 1 bottle of rum and fill 30 points of health',
        'whereami' => 'shows current harbor',
        'exit' => 'finishes the game'
    ];

    public const MAX_ARMOUR = 10;
    public const MAX_LUCK = 10;
    public const MAX_STRENGTH = 10;
    public const MAX_HEALTH = 100;
    public const PLAYER_BASE_HEALTH = 60;
    public const MAX_HOLD = 3;
    public const RUM_HEALING = 30;

    public const VESSELS = [
        'player' => [
            'name' => 'Player',
            'strength' => 4,
            'armour' => 4,
            'luck' => 4,
            'health' => 60,
            'hold' => ['_', '_', '_']
        ],
        'schooner' => [
            'name' => 'Royal Patrool Schooner',
            'strength' => ['min' => 2, 'max' => 4],
            'armour' => ['min' => 2, 'max' => 4],
            'luck' => ['min' => 1, 'max' => 4],
            'health' => 50,
            'hold' => ['💰', '_', '_']
        ],
        'battle' => [
            'name' => 'Royal Battle Ship',
            'strength' => ['min' => 4, 'max' => 8],
            'armour' => ['min' => 4, 'max' => 8],
            'luck' => ['min' => 4, 'max' => 7],
            'health' => 80,
            'hold' => ['🍾', '_', '_']
        ],
        'royal' => [
            'name' => 'HMS Royal Sovereign',
            'strength' => 10,
            'armour' => 10,
            'luck' => 10,
            'health' => 100,
            'hold' => ['💰', '💰', '🍾']
        ]
    ];

    public const HOME = 1;

    public const MAP = [
        1 => [
            'harbor' => 1,
            'name' => 'Pirates Harbor',
            'enemy' => null,
            'sail' => [
                'east' => 0,
                'west' => 3,
                'north' => 4,
                'south' => 2,
            ]
        ],
        2 => [
            'harbor' => 2,
            'name' => 'Southhampton',
            'enemy' => 'schooner',
            'sail' => [
                'east' => 7,
                'west' => 3,
                'north' => 1,
                'south' => 0,
            ]
        ],
        3 => [
            'harbor' => 3,
            'name' => 'Fishguard',
            'enemy' => 'schooner',
            'sail' => [
                'east' => 1,
                'west' => 0,
                'north' => 4,
                'south' => 2,
            ]
        ],
        4 => [
            'harbor' => 4,
            'name' => 'Salt End',
            'enemy' => 'schooner',
            'sail' => [
                'east' => 5,
                'west' => 3,
                'north' => 0,
                'south' => 1,
            ]
        ],
        5 => [
            'harbor' => 5,
            'name' => 'Isle of Grain',
            'enemy' => 'schooner',
            'sail' => [
                'east' => 6,
                'west' => 4,
                'north' => 0,
                'south' => 7,
            ]
        ],
        6 => [
            'harbor' => 6,
            'name' => 'Grays',
            'enemy' => 'battle',
            'sail' => [
                'east' => 0,
                'west' => 5,
                'north' => 0,
                'south' => 8,
            ]
        ],
        7 => [
            'harbor' => 7,
            'name' => 'Felixstowe',
            'enemy' => 'battle',
            'sail' => [
                'east' => 8,
                'west' => 2,
                'north' => 5,
                'south' => 0,
            ]
        ],
        8 => [
            'harbor' => 8,
            'name' => 'London Docks',
            'enemy' => 'royal',
            'sail' => [
                'east' => 0,
                'west' => 7,
                'north' => 6,
                'south' => 0,
            ]
        ]
    ];
}

<?php

namespace BinaryStudioAcademy\Game\Io;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;

class CliReader implements Reader
{
    private $stream;

    public function __construct()
    {
        $this->stream = STDIN;
    }

    public function read(): string
    {
        $line = fgets($this->stream);
        $string = strtolower(preg_replace("/\s{2,}/", ' ', trim($line, " .\t\n\r\0\x0B")));
        return $string;
    }

    public function getStream()
    {
        return $this->stream;
    }
}

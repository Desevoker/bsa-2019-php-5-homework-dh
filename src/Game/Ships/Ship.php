<?php

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Contracts\Ships\Ship as IShip;
use BinaryStudioAcademy\Game\Helpers\World;

class Ship implements IShip
{
    private $name;
    private $strength;
    private $armour;
    private $luck;
    private $health;
    private $hold;
    private $dead = false;
    private $maxStrength = World::MAX_STRENGTH;
    private $maxArmour = World::MAX_ARMOUR;
    private $maxLuck = World::MAX_LUCK;
    private $maxHealth = World::MAX_HEALTH;
    private $maxHold = World::MAX_HOLD;
    private $playerBaseHealth = World::PLAYER_BASE_HEALTH;

    public function getName(): string
    {
        return $this->name;
    }

    public function getStrength(): int
    {
        return $this->strength;
    }

    public function getArmour(): int
    {
        return $this->armour;
    }

    public function getLuck(): int
    {
        return $this->luck;
    }

    public function getHealth(): int
    {
        return $this->health;
    }

    public function getHold(): array
    {
        return $this->hold;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setStrength(int $strength): void
    {
        if ($strength > $this->maxStrength) {
            throw new \InvalidArgumentException("Ship strength cannot be greater than {$this->maxStrength}");
        }

        $this->strength = $strength;
    }

    public function setArmour(int $armour): void
    {
        if ($armour > $this->maxArmour) {
            throw new \InvalidArgumentException("Ship armour cannot be greater than {$this->maxArmour}");
        }

        $this->armour = $armour;
    }

    public function setLuck(int $luck): void
    {
        if ($luck > $this->maxLuck) {
            throw new \InvalidArgumentException("Ship luck cannot be greater than {$this->maxLuck}");
        }

        $this->luck = $luck;
    }

    public function setHealth(int $health): void
    {
        if ($health > $this->maxHealth) {
            throw new \InvalidArgumentException("Ship health cannot be greater than {$this->maxHealth}");
        }

        $this->health = $health;
    }

    public function setHold(array $hold): void
    {
        if (count($hold) > $this->maxHold) {
            throw new \InvalidArgumentException("Ship cannot hold more than {$this->maxHold} items");
        }

        $this->hold = $hold;
    }

    public function heal(?int $ammount = null): void
    {
        if ($ammount === null) {
            $this->setHealth($this->playerBaseHealth);
            $this->respawn();
        } else {
            if ($ammount < 0) {
                throw new \InvalidArgumentException('Healing cannot be lesser than 0');
            }

            if ($this->getHealth() + $ammount >= $this->maxHealth) {
                $this->setHealth($this->maxHealth);
            } else {
                $this->health += $ammount;
            }
        }
    }

    public function receiveDamage(int $damage): void
    {
        if ($damage < 0) {
            throw new \InvalidArgumentException('Damage cannot be lesser than 0');
        }

        $this->health -= $damage;

        if ($this->health <= 0) {
            $this->health = 0;
            $this->sunk();
        }
    }

    public function isAlive(): bool
    {
        return !$this->dead;
    }

    public function isDead(): bool
    {
        return $this->dead;
    }

    private function respawn(): void
    {
        $this->dead = false;
    }

    private function sunk(): void
    {
        $this->dead = true;
    }
}

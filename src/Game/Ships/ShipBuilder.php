<?php

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Contracts\Ships\Ship as IShip;
use BinaryStudioAcademy\Game\Contracts\Ships\ShipBuilder as IShipBuilder;
use BinaryStudioAcademy\Game\Helpers\World;

class ShipBuilder implements IShipBuilder
{
    private $random;
    private $ship;
    private $stats;
    private $vessels;

    public function __construct(Random $random)
    {
        $this->vessels = World::VESSELS;
        $this->random = $random;
    }

    public function buildShip(string $type): IShip
    {
        $this->setType($type);
        $this->createShip();
        $this->setName();
        $this->setStrength();
        $this->setArmour();
        $this->setLuck();
        $this->setHealth();
        $this->setHold();
        return $this->getShip();
    }

    private function setType(string $type): void
    {
        if (!array_key_exists($type, $this->vessels)) {
            throw new \InvalidArgumentException("Can't build ship of type {$type}");
        }

        $this->stats = $this->vessels[$type];
    }

    private function createShip(): void
    {
        $this->ship = new Ship;
    }

    private function setName(): void
    {
        $this->ship->setName($this->stats['name']);
    }

    private function setStrength(): void
    {
        if (is_array($this->stats['strength'])) {
            $min = $this->stats['strength']['min'];
            $max = $this->stats['strength']['max'];
            $strength = round($min + $this->random->get() * ($max - $min));
            $this->ship->setStrength($strength);
        } else {
            $this->ship->setStrength($this->stats['strength']);
        }
    }

    private function setArmour(): void
    {
        if (is_array($this->stats['armour'])) {
            $min = $this->stats['armour']['min'];
            $max = $this->stats['armour']['max'];
            $armour = round($min + $this->random->get() * ($max - $min));
            $this->ship->setArmour($armour);
        } else {
            $this->ship->setArmour($this->stats['armour']);
        }
    }

    private function setLuck(): void
    {
        if (is_array($this->stats['luck'])) {
            $min = $this->stats['luck']['min'];
            $max = $this->stats['luck']['max'];
            $luck = round($min + $this->random->get() * ($max - $min));
            $this->ship->setLuck($luck);
        } else {
            $this->ship->setLuck($this->stats['luck']);
        }
    }

    private function setHealth(): void
    {
        $this->ship->setHealth($this->stats['health']);
    }

    private function setHold(): void
    {
        $this->ship->setHold($this->stats['hold']);
    }

    private function getShip(): IShip
    {
        return $this->ship;
    }
}
